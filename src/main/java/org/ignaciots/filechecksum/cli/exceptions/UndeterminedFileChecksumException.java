package org.ignaciots.filechecksum.cli.exceptions;

/**
 * Exception thrown when the FileChecksum instance cannot be determined:
 * i.e. the MessageDigestAlgorithm cannot be determined
 * @author Ignacio Torre
 * @version 1.0
 */

public class UndeterminedFileChecksumException extends Exception {

	private static final long serialVersionUID = -2809919686058893810L;

	public UndeterminedFileChecksumException() {
		super();
	}

	public UndeterminedFileChecksumException(String message) {
		super(message);
	}

	public UndeterminedFileChecksumException(Throwable cause) {
		super(cause);
	}

	public UndeterminedFileChecksumException(String message, Throwable cause) {
		super(message, cause);
	}

	public UndeterminedFileChecksumException(String message, Throwable cause, boolean enableSupression,
			boolean writableStackTrace) {
		super(message, cause, enableSupression, writableStackTrace);
	}

}
