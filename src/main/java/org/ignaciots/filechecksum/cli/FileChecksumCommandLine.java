package org.ignaciots.filechecksum.cli;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import org.apache.commons.cli.AlreadySelectedException;
import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.UnrecognizedOptionException;
import org.ignaciots.filechecksum.checksum.FileChecksum;
import org.ignaciots.filechecksum.checksum.algorithm.impl.crc.CRC32Algorithm;
import org.ignaciots.filechecksum.checksum.algorithm.impl.digest.*;
import org.ignaciots.filechecksum.cli.exceptions.UndeterminedFileChecksumException;
import org.ignaciots.filechecksum.cli.helper.OptionHelper;
import org.ignaciots.filechecksum.cli.parser.ArgumentParser;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

/**
 * Command line program that calculates the checksum of a file with a specific hashing algorithm
 * @author Ignacio Torre
 * @version 1.0
 */

public class FileChecksumCommandLine {
	
	public static final String PROGRAM_NAME = "checksum";
	public static final String PROGRAM_LONG_NAME = "File Checksum";
	public static final String PROGRAM_AUTHOR = "Ignacio Torre";
	public static final String PROGRAM_VERSION = "1.0.0";
	public static final String PROGRAM_USAGE = "[-h] [-v] -m2 | -m5 | -s1 | -s22 | -s25 | -s3 | -s5 | -c3 files";
	public static final int INVALID_ARG_EXIT_CODE = 3; // Exit code for invalid arguments
	public static final int INVALID_ALGORITHM_EXIT_CODE = 4; //Exit code for invalid algorithm
	public static final int UNDETERMINED_CHECKSUM_EXIT_CODE = 5; //Exit code for undetermined FileChecksum
	public static final int FILE_NOT_FOUND_EXIT_CODE = 6; //Exit code for file not found
	public static final int IO_ERROR_EXIT_CODE = 7; //Exit code for general IO error
	
	private final String[] args;
	private final Options options = OptionHelper.getOptions();
	
	
	public FileChecksumCommandLine(String[] args) {
		this.args = args;
	}
	
	/**
	 * Runs the File Checksum CLI program
	 */
	public void run() {
		CommandLine commandLine = null;
		
		try {
			commandLine = new ArgumentParser(args, options).parse();
		} catch (AlreadySelectedException e) {
			handleAlreadySelectedException(e);
		} catch (MissingArgumentException e) {
			handleMissingArgumentException(e);
		} catch (MissingOptionException e) {
			handleMissingOptionException(e);
		} catch (UnrecognizedOptionException e) {
			handleUnrecognizedOptionException(e);
		} catch (ParseException e) {
			handleParseException(e);
		}
		
		if (commandLine.hasOption(OptionHelper.HELP_OPTION_SHORTNAME)) {
			showHelp();
		}
		if (commandLine.hasOption(OptionHelper.VERSION_OPTION_SHORTNAME)) {
			showVersion();
		}
		
		if (commandLine.getArgs().length < 1) { //checking positional arguments
			handleMissingPositionalArgument();
		}
		
		for (String filename : commandLine.getArgs()) {
			File file = new File(filename);
			try {
				if (commandLine.hasOption(OptionHelper.ALL_OPTION_SHORTNAME)) {
					showAllChecksums(file);
				} else if (commandLine.hasOption(OptionHelper.MD2_OPTION_SHORTNAME)) {
					showMD2Checksum(file);
				} else if (commandLine.hasOption(OptionHelper.MD5_OPTION_SHORTNAME)) {
					showMD5Checksum(file);
				} else if (commandLine.hasOption(OptionHelper.SHA1_OPTION_SHORTNAME)) {
					showSHA1Checksum(file);
				} else if (commandLine.hasOption(OptionHelper.SHA224_OPTION_SHORTNAME)) {
					showSHA224Checksum(file);
				} else if (commandLine.hasOption(OptionHelper.SHA256_OPTION_SHORTNAME)) {
					showSHA256Checksum(file);
				} else if (commandLine.hasOption(OptionHelper.SHA384_OPTION_SHORTNAME)) {
					showSHA384Checksum(file);
				} else if (commandLine.hasOption(OptionHelper.SHA512_OPTION_SHORTNAME)) {
					showSHA512Checksum(file);
				} else if (commandLine.hasOption(OptionHelper.CRC32_OPTION_SHORTNAME)) {
					showCRC32Checksum(file);
				} else {
					throw new UndeterminedFileChecksumException(
							"Cannot create FileChecksum: undetermined MessageDigestAlgorithm");
				}
			} catch (NoSuchAlgorithmException e) {
				handleNoSuchAlgorithmException(e);
			} catch (UndeterminedFileChecksumException e) {
				handleUndeterminedFileChecksumException(e);
			}
		}
	}

	private void handleAlreadySelectedException(AlreadySelectedException e) {
		String cause = e.getOption().getOpt();
		String optionGroup = OptionHelper.prettyOptionGroup(e.getOptionGroup());
		System.err.println(
				String.format("Error: invalid option: %s: More than one of the following options has been chosen: %s\n"
						+ "Use %s -h for more help.", cause, optionGroup, PROGRAM_NAME));
		System.exit(INVALID_ARG_EXIT_CODE);
	}
	
	private void handleMissingArgumentException(MissingArgumentException e) {
		String cause = e.getOption().getOpt();
		System.err.println(String.format("Error: argument for option -%s has not been provided\n" + "Use %s -h for more help.",
				cause, PROGRAM_NAME));
		System.exit(INVALID_ARG_EXIT_CODE);
	}
	
	private void handleMissingOptionException(MissingOptionException e) {
		String cause = OptionHelper.prettyMissingOptions(e.getMissingOptions());
		System.err.println(String.format("Error: one option from each of the following option groups is missing: %s\n" + "Use %s -h for more help.", cause,
				PROGRAM_NAME));
		System.exit(INVALID_ARG_EXIT_CODE);
	}
	
	private void handleUnrecognizedOptionException(UnrecognizedOptionException e) {
		String cause = e.getOption();
		System.err.println(
				String.format("Error: unrecognized options: %s\n" + "Use %s -h for more help.", cause, PROGRAM_NAME));
		System.exit(INVALID_ARG_EXIT_CODE);
	}
	
	private void handleParseException(ParseException e) {
		e.printStackTrace();
		System.err.println("Error: fatal error parsing arguments.\nExiting...");
		System.exit(INVALID_ARG_EXIT_CODE);
	}
	
	private void handleNoSuchAlgorithmException(NoSuchAlgorithmException e) {
		e.printStackTrace();
		System.err.println("Error: fatal error, invalid checksum algorithm.\nExiting... ");
		System.exit(INVALID_ALGORITHM_EXIT_CODE);
	} 
	
	private void handleUndeterminedFileChecksumException(UndeterminedFileChecksumException e) {
		e.printStackTrace();
		System.err.println("Error: fatal error, checksum algorithm cannot be determined.\nExiting...");
		System.exit(UNDETERMINED_CHECKSUM_EXIT_CODE);
	}
	
	private void handleFileNotFoundException(FileNotFoundException e, File file) {
		System.err.println(String.format("Error: file \"%s\" not found.", file.getAbsolutePath()));
		System.exit(FILE_NOT_FOUND_EXIT_CODE);
	}
	
	private void handleIOException(IOException e, File file) {
		System.err.println(String.format("Error: file opening file \"%s\"", file.getAbsolutePath()));
		System.exit(IO_ERROR_EXIT_CODE);
	}
	
	private void handleMissingPositionalArgument() {
		System.err.println(String.format("Error: missing positional argument: %s\nUse %s -h for more help.",
				OptionHelper.FILE_POSITIONAL_NAME, PROGRAM_NAME));
		System.exit(INVALID_ARG_EXIT_CODE);
	}
	
	private void showHelp() {
		StringBuilder builder = new StringBuilder();
		builder.append(String.format("%s usage:\n%s %s\n", PROGRAM_NAME, PROGRAM_NAME, PROGRAM_USAGE));
		for (Option option : this.options.getOptions()) {
			builder.append(OptionHelper.prettyOption(option));
			builder.append("\n");
		}
		builder.append(
				String.format("%s: %s", OptionHelper.FILE_POSITIONAL_NAME, OptionHelper.FILE_POSITIONAL_DESCRIPTION));
		System.out.println(builder.toString());
		System.exit(0);
	}
	
	private void showVersion() {
		System.out.println(String.format("%s version %s\nCopyright \u00A9 %s", PROGRAM_LONG_NAME, PROGRAM_VERSION,
				PROGRAM_AUTHOR));
		System.exit(0);
	}
	
	private void showMD2Checksum(File file) throws NoSuchAlgorithmException {
		FileChecksum checksum = new FileChecksum(new MD2Algorithm());
		showChecksum(checksum, file);
	}
	
	private void showMD5Checksum(File file) throws NoSuchAlgorithmException {
		FileChecksum checksum = new FileChecksum(new MD5Algorithm());
		showChecksum(checksum, file);
	}
	
	private void showSHA1Checksum(File file) throws NoSuchAlgorithmException {
		FileChecksum checksum = new FileChecksum(new SHA1Algorithm());
		showChecksum(checksum, file);
	}
	
	private void showSHA224Checksum(File file) throws NoSuchAlgorithmException {
		FileChecksum checksum = new FileChecksum(new SHA224Algorithm());
		showChecksum(checksum, file);
	}
	
	private void showSHA256Checksum(File file) throws NoSuchAlgorithmException {
		FileChecksum checksum = new FileChecksum(new SHA256Algorithm());
		showChecksum(checksum, file);
	}
	
	private void showSHA384Checksum(File file) throws NoSuchAlgorithmException {
		FileChecksum checksum = new FileChecksum(new SHA384Algorithm());
		showChecksum(checksum, file);
	}
	
	private void showSHA512Checksum(File file) throws NoSuchAlgorithmException {
		FileChecksum checksum = new FileChecksum(new SHA512Algorithm());
		showChecksum(checksum, file);
	}
	
	private void showCRC32Checksum(File file) throws NoSuchAlgorithmException {
		FileChecksum checksum = new FileChecksum(new CRC32Algorithm());
		showChecksum(checksum, file);
	}
	
	private void showAllChecksums(File file) throws NoSuchAlgorithmException {
		showMD2Checksum(file);
		showMD5Checksum(file);
		showSHA1Checksum(file);
		showSHA224Checksum(file);
		showSHA256Checksum(file);
		showSHA384Checksum(file);
		showSHA512Checksum(file);
		showCRC32Checksum(file);
	}
	
	private void showChecksum(FileChecksum fileChecksum, File file) {
		byte[] checksum = null;
		try {
			checksum = fileChecksum.checksum(file);
		} catch (FileNotFoundException e) {
			handleFileNotFoundException(e, file);
		} catch (IOException e) {
			handleIOException(e, file);
		}
		
		System.out.println(String.format("[%s] %s checksum: %s", file.getName(),
				fileChecksum.getAlgorithm().getAlgorithmName(), toHexString(checksum)));
	}
	
	private String toHexString(byte[] checksum) {
		StringBuilder hexString = new StringBuilder();
		for (int i = 0; i < checksum.length; i++) {
			hexString.append(Integer.toHexString(0xFF & checksum[i]));
		}
		
		return hexString.toString();
	}

}
