package org.ignaciots.filechecksum.cli.parser;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * Class used to parse the arguments given to the FileChecksum program
 * @author Ignacio Torre
 * @version 1.0
 */

public class ArgumentParser {
	
	private final String[] args;
	private final Options options;
	
	/**
	 * Creates a new argument parser 
	 * @param args The arguments given to the argument parser
	 */
	public ArgumentParser(String[] args, Options options) {
		this.args = args;
		this.options = options;
	}
	
	/**
	 * Parses the given arguments to the argument parser 
	 * @return A CommandLine object containing the parsed options
	 * @throws ParseException Thrown when an error occurs parsing the given arguments
	 */
	public CommandLine parse() throws ParseException{
		return new DefaultParser().parse(options, args);
	}
	
	

}