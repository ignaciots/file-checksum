package org.ignaciots.filechecksum.checksum;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.zip.CRC32;

import org.ignaciots.filechecksum.checksum.algorithm.ChecksumAlgorithm;
import org.ignaciots.filechecksum.checksum.algorithm.types.AlgorithmType;

/**
 * Class that performs a checksum of a given file with a specific algorithm
 * @author Ignacio Torre
 * @version 1.0
 */

public class FileChecksum {
	
	private final ChecksumAlgorithm checksumAlgorithm;
	private MessageDigest messageDigest;
	private CRC32 crc32;
	
	/**
	 * Creates a FileChecksum with a specific ChecksumAlgorithm algorithm
	 * @param algorithm The ChecksumAlgorithm algorithm used to calculate the file checksum
	 * @throws NoSuchAlgorithmException Thrown when the requested algorithm is not supported
	 */
	public FileChecksum(ChecksumAlgorithm algorithm) throws NoSuchAlgorithmException {
		if (algorithm == null) {
			throw new IllegalArgumentException("The MessageDigestAlgorithm cannot be null");
		}
		this.checksumAlgorithm = algorithm;
		
		setCurrentAlgorithm();
	}
	
	/**
	 * Calculates the checksum of a given file with a previuos specified algorithm
	 * @param file The file used to calculate the checksum
	 * @return An array of bytes representing the resulting file checksum
	 * @throws FileNotFoundException Throw when the given file is not found by the system
	 * @throws IOException Thrown when a system general I/O error occurs
	 */
	public byte[] checksum(File file) throws FileNotFoundException, IOException {
		if (file == null) {
			throw new IllegalArgumentException("Checksum File must not be null");
		}


		if (messageDigest != null) {
			try (BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(file))) {
				byte[] readDataBuffer = new byte[4096];
				int readData;

				while ((readData = inputStream.read(readDataBuffer)) != -1) {
					messageDigest.update(readDataBuffer, 0, readData);
				}

				return messageDigest.digest();
			}
		}
		try (BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(file))) {
			byte[] readDataBuffer = new byte[4096];
			int readData;

			while ((readData = inputStream.read(readDataBuffer)) != -1) {
				crc32.update(readDataBuffer, 0, readData);
			}

			long checksumValue = crc32.getValue();
			ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
		    buffer.putLong(checksumValue);
		    return buffer.array();
		}
	}
		

	/**
	 * Gets the checksum algorithm used in the current instance of FileChecksum
	 * @return The ChecksumAlgorithm used in the file checksum
	 */
	public ChecksumAlgorithm getAlgorithm() {
		return this.checksumAlgorithm;
	}
	
	private void setCurrentAlgorithm() throws NoSuchAlgorithmException {
		AlgorithmType algorithmType = checksumAlgorithm.getAlgorithmType();
		if (algorithmType == AlgorithmType.DIGEST) {
			messageDigest = MessageDigest.getInstance(checksumAlgorithm.getAlgorithmName());
		} else if (algorithmType == AlgorithmType.CRC) {
			crc32 = new CRC32();
		} else {
			throw new IllegalArgumentException(
					String.format("Unsupported algorithm type: %s", checksumAlgorithm.getAlgorithmType().name()));
		}
	}

}
