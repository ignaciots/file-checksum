package org.ignaciots.filechecksum.checksum.algorithm.impl.crc;

import org.ignaciots.filechecksum.checksum.algorithm.CRCAlgorithm;
import org.ignaciots.filechecksum.checksum.algorithm.types.AlgorithmType;

/**
 * CRC32 algorithm
 * @author Ignacio Torre
 * @version 1.0
 */

public class CRC32Algorithm extends CRCAlgorithm {
	
	private final String algorithmName = "CRC32";

	@Override
	public AlgorithmType getAlgorithmType() {
		return AlgorithmType.CRC;
	}

	@Override
	public String getAlgorithmName() {
		return this.algorithmName;
	}

}
