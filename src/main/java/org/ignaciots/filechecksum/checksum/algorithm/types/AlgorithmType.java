package org.ignaciots.filechecksum.checksum.algorithm.types;

/**
 * Enum that represents algorithm types
 * @author Ignacio Torre
 * @version 1.0
 */

public enum AlgorithmType {
	CRC,
	DIGEST;
}
