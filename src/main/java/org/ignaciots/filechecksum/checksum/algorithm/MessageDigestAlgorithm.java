package org.ignaciots.filechecksum.checksum.algorithm;

/**
 * Class that represents one of the MessageDigest algorithms
 * @author Ignacio Torre
 * @version 1.0
 */

public abstract class MessageDigestAlgorithm implements ChecksumAlgorithm { }
