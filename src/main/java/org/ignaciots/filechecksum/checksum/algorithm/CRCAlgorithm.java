package org.ignaciots.filechecksum.checksum.algorithm;

/**
 * Class that represents a cyclic redundancy check algorithm
 * @author Ignacio Torre
 * @version 1.0
 */

public abstract class CRCAlgorithm implements ChecksumAlgorithm { }
