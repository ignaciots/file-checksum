package org.ignaciots.filechecksum.checksum.algorithm;

import org.ignaciots.filechecksum.checksum.algorithm.types.AlgorithmType;

/**
 * Interface used for various checksum algorithms
 * @author Ignacio Torre
 * @version 1.0
 */

public interface ChecksumAlgorithm {

	/**
	 * Gets the algorithm type
	 * @return The type of the checksum algorithm
	 */
	AlgorithmType getAlgorithmType();
	
	/**
	 * Gets the name of the associated algorithm
	 * @return The name of the associated algorithm, usable to obtain a MessageDigest instance
	 */
	String getAlgorithmName();
}
