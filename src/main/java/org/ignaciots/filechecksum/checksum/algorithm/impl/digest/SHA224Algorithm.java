package org.ignaciots.filechecksum.checksum.algorithm.impl.digest;

import org.ignaciots.filechecksum.checksum.algorithm.MessageDigestAlgorithm;
import org.ignaciots.filechecksum.checksum.algorithm.types.AlgorithmType;

/**
 * MessageDigest SHA1 algorithm
 * @author Ignacio Torre
 * @version 1.0
 */

public class SHA224Algorithm extends MessageDigestAlgorithm {

	private final String algorithmName = "SHA-224";
	
	@Override
	public String getAlgorithmName() {
		return this.algorithmName;
	}
	
	@Override
	public AlgorithmType getAlgorithmType() {
		return AlgorithmType.DIGEST;
	}

}
