package org.ignaciots.filechecksum.main;

import org.ignaciots.filechecksum.cli.FileChecksumCommandLine;

/**
 * Main class
 * @author Ignacio Torre
 * @version 1.0
 */

public class Main {

	public static void main(String[] args) {
		new FileChecksumCommandLine(args).run();
	}

}
